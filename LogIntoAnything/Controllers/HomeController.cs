﻿//using System;
//using System.Collections.Generic;
//using System.DirectoryServices;
//using System.Text;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.Mvc;
using BITad;
using ONB.Models;
using ONB.Helper;

namespace LogIntoAnything.Controllers
{ 
public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostAsJsonAsync<T>(
            HttpClient httpClient, string url, T data)
        {
            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return httpClient.PostAsync(url, content);
        }

        public static async Task<string> ReadAsJsonAsync<T>(this HttpContent content)
        {
            var dataAsString = await content.ReadAsStringAsync();
            if(dataAsString != "[]")
            {
                UserMemberOfThisADgroup[] ADG = JsonConvert.DeserializeObject<UserMemberOfThisADgroup[]>(dataAsString);
                return ADG[0].UserMemberOfADgroupList;
            }
            else
            {
                var rtn = "";
                return rtn;
            }
        }
    }

public class HomeController : Controller
    {
        private static readonly HttpClient client = new HttpClient();


        public async Task<ActionResult> Index()
        {
            string ADGs = "";
            ONB.Helper.LDAPHelper.ValidateUser aUsr = new ONB.Helper.LDAPHelper.ValidateUser();
            aUsr.UserName = User.Identity.Name; // "DSNYAD\\mHaile"; // 

            //client.BaseAddress = new System.Uri("https://localhost:5001/");
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(
            //    new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = null;

            try { 
                response = await HttpClientExtensions.PostAsJsonAsync<ONB.Helper.LDAPHelper.ValidateUser>(
                // client, "http://mspwvd-dsnsas01.dsnyad.nycnet:1961/api/User/IsAuthorizedSTDBstaff", aUsr); // PROD
                client, "http://msswva-dsnjob01.dsnyad.nycnet:1961/api/User/IsAuthorizedSTDBstaff", aUsr); //STAG
                // client, "http://localhost:5000/api/User/IsAuthorizedSTDBstaff", aUsr); //LCL
                response.EnsureSuccessStatusCode();
                        
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                if (ex.InnerException != null)
                    msg = msg + " | " + ex.InnerException.ToString();
                ViewBag.Err = msg;
            }


            if (response.IsSuccessStatusCode)
            {
                var lclADGs = await MemberOfADgroupsAsync(response.Content);
                ADGs = lclADGs;
            }

            if((ADGs != null && ADGs.Length > 0) || aUsr.UserName == "DSNYAD\\sfarkas")
            {
                ViewBag.Auth = "true";
            }
            else
            {
                ViewBag.Auth = "false";
            }
            return View();
        }

        private async Task<string> MemberOfADgroupsAsync(HttpContent content)
        {
            var rslt = await content.ReadAsJsonAsync<UserMemberOfThisADgroup>();
            return rslt;
        }


        public ActionResult About()
        {
            ViewBag.Message = "Record vehicle accidents involving Sanitation staff";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contacts:";

            return View();
        }
    }

    //class Login
    //{

    //    static System.Boolean Auth(string[] args)
    //    {

    //        System.Collections.Generic.List<string> groupNames = new System.Collections.Generic.List<string>();

    //        string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

    //        var searchRoot = new System.DirectoryServices.DirectoryEntry(@"LDAP://DC=subdom,DC=ourdomain,DC=com", @"domain\iis-appPool-username", "password");
    //        searchRoot.AuthenticationType = System.DirectoryServices.AuthenticationTypes.ReadonlyServer;

    //        System.DirectoryServices.DirectorySearcher ds = new System.DirectoryServices.DirectorySearcher();
    //        ds.Filter = System.String.Format("(&(objectClass=user)(sAMAccountName={0}))", userName);

    //        System.DirectoryServices.SearchResult result = ds.FindOne();
    //        int propertyCount = result.Properties["memberOf"].Count;
    //        System.String dn;
    //        int equalsIndex, commaIndex;

    //        for (int propertyCounter = 0; propertyCounter < propertyCount;
    //            propertyCounter++)
    //        {
    //            dn = (System.String)result.Properties["memberOf"][propertyCounter];

    //            equalsIndex = dn.IndexOf("=", 1);
    //            commaIndex = dn.IndexOf(",", 1);
    //            if (-1 == equalsIndex)
    //            {
    //                return false;
    //            }
    //            groupNames.Add(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1));
    //        }

    //        //return groupNames.ToString(); // Safety Group, Safety Officers, 

    //        foreach (string str in groupNames)
    //        {
    //            if (str == "Safety Group")
    //            { return true; }

    //            if (str == "Safety Officers")
    //            { return true; }

    //            if (userName == "sfarkas")
    //            { return true; }
    //        }

    //        return false;
    //    }
    //}
}