﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONB.Models
{
    public class MemberOfADgroups
    {
        public string UserName
        {
            get; set;
        }
        public List<UserMemberOfThisADgroup> UserMemberOfADgroupList
        {
            get; set;
        }

        public MemberOfADgroups GetADgroups( ONB.Helper.LDAPHelper.ValidateUser objUser )
        {
            MemberOfADgroups lADgroups = new MemberOfADgroups()
            {
                UserName = objUser.UserName,
                UserMemberOfADgroupList = new List<UserMemberOfThisADgroup>(),
            };
            return lADgroups;
        }

        public static explicit operator MemberOfADgroups(Task<string> v)
        {
            throw new NotImplementedException();
        }
    }

    public class UserMemberOfThisADgroup
    {
        public string UserName
        {
            get; set;
        }
        public string UserMemberOfADgroupList
        {
            get; set;
        }
    }


}