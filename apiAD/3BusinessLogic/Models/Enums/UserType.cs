﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Onboarding.API.Models.Enums
{
    public enum UserTypeEnum : int
    {
        [Description( "New User" )]
        newUser = 1,

        [Description( "Order new desk phone" )]
        existingUser = 2,


    }
}
