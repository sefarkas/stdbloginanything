﻿namespace ONB.Models
{
    public class UserRoleType
    {
        public int UserRoleTypeId
        {
            get; set;
        }
        public int RoleTypeId
        {
            get; set;
        }
        public string RoleTypeName
        {
            get; set;
        }
        public int ApplicationId
        {
            get; set;
        }
        public string ApplicationName
        {
            get; set;
        }
    }
}