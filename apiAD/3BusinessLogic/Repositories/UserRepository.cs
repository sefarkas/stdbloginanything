﻿using System;
using ONB.DataAccess;
using ONB.Helper;
using static ONB.Helper.LDAPHelper;

namespace ONB.Services
{
    public class UserRepository
    {
        public ONB.Models.Users RepositoryGetOneUser( ValidateUser objUser )
        {
            ONB.Models.Users reposUser = new ONB.Models.Users();
            // strResult = LDAPHelper.GetOneString( objUser.UserName );
            LDAPHelper objHelper = new LDAPHelper();
            reposUser.UserName = objUser.UserName;
            reposUser.Password = objUser.Password;
            reposUser.AuthenticationMessage = RepositoryGetOneString( objUser );
            return reposUser;
        }

        public string RepositoryGetOneString( ValidateUser objUser )
        {
            ONB.Models.Users reposUser = new ONB.Models.Users();
            // strResult = LDAPHelper.GetOneString( objUser.UserName );
            LDAPHelper objHelper = new LDAPHelper();
            return objHelper.GetAuthenticationMessage( objUser ).AuthenticationMessage;
        }

        public bool ValidateUserLogin( ValidateUser objuser )
        {
            bool authenticationError = false;
            try
            {
                LDAPHelper lHelper = new LDAPHelper();
                authenticationError = lHelper.ValidateUserCredentials( objuser );
            }
            catch (Exception ex)
            {
                //todo log exceptions
                Common.LogException( ex );
            }
            return authenticationError;
        }

        public ONB.Models.Users RepositoryGetUserAttributes( ONB.Models.Users objuser )
        {
            ONB.Models.Users authenticationError = new ONB.Models.Users();
            try
            {
                LDAPHelper lHelper = new LDAPHelper();
                authenticationError = lHelper.GetUserAttributesHelper( objuser );
            }
            catch (Exception ex)
            {
                //todo log exceptions
                Common.LogException( ex );
            }
            return authenticationError;
        }
    }
}
