﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSwag.AspNetCore;

//using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;


namespace BITadSvc
{
    public class Startup
    {
        public Startup( IConfiguration configuration )
        {
            Configuration = configuration;
        }

        readonly string BITadAllowSpecificOrigins = "_BITadAllowSpecificOrigins";


        public IConfiguration Configuration
        {
            get;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices( IServiceCollection services )
        {
            services.AddCors( options =>
            {
                options.AddPolicy( "CorsPolicy", builder =>
                     builder
                     .AllowAnyOrigin()
                     .AllowAnyMethod()
                     .AllowAnyHeader()
                     .AllowCredentials()
                     .WithExposedHeaders( "Token-Expired" ) );
            } );

            services.AddSwaggerDocument( configure =>
            {
                configure.PostProcess = document =>
                {
                    document.Info.Version = "v0a";
                    document.Info.Title = "BIT Active Directory API";
                };
            } );

            //services.AddAuthenticationCore(
            //    o => o.DefaultScheme = JwtBearerDefaults.AuthenticationScheme )
            //    .AddJwtBearer( options =>
            //    {
            //        options.TokenValidationParameters = new TokenValidationParameters
            //        {
            //            ValidateAudience = false,
            //            ValidateIssuer = false,
            //            ValidateIssuerSigningKey = true,
            //            IssuerSigningKey = new SymmetricSecurityKey( Encoding.UTF8.GetBytes( Configuration["serverSigningPassword"] ) ),
            //            ValidateLifetime = true,
            //            ClockSkew = TimeSpan.FromMinutes( 1 ) //TODO use the default for this setting is 5 minutes
            //                    };

            //        options.Events = new JwtBearerEvents
            //        {
            //            OnAuthenticationFailed = context =>
            //            {
            //                if (context.Exception.GetType() == typeof( SecurityTokenExpiredException ))
            //                {
            //                    context.Response.Headers.Add( "Token-Expired", "true" );
            //                }
            //                return Task.CompletedTask;
            //            }
            //        };
            //    } );


            services.AddCors( options =>
            {
                options.AddPolicy( BITadAllowSpecificOrigins,
                builder =>
                    builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .WithExposedHeaders( "Token-Expired" ) );
            } );

            services.AddMvc().SetCompatibilityVersion( CompatibilityVersion.Version_2_1 );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IHostingEnvironment env )
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors( BITadAllowSpecificOrigins );

            // app.UseAuthentication();
            app.UseCors( "CorsPolicy" );
            // _ = app.UseSwagger();
            app.UseOpenApi();
            app.UseSwaggerUi3();


            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
