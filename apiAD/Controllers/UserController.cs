﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ONB.Services;
using ONB.Helper;
using System.Web.Http.Cors;  // for cross-domain browsing

namespace BITad.Controllers
/*
 * Published to BITadSvcPublishTo10_155_230_134_WinSvr2016
 * Sample URI: http://10.155.230.134:44389/api/User/ValidateUserCredentials
 * */
{
    [EnableCors( origins: "*", headers: "*", methods: "*" )] // for cross-domain browsing
    [Microsoft.AspNetCore.Mvc.Route( "api/[controller]" )]
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details( int id )
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( IFormCollection collection )
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction( nameof( Index ) );
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit( int id )
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( int id, IFormCollection collection )
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction( nameof( Index ) );
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete( int id )
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete( int id, IFormCollection collection )
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction( nameof( Index ) );
            }
            catch
            {
                return View();
            }
        }


        private UserRepository userRepo;
        public UserController()
        {
            userRepo = new UserRepository();
        }

        [Route( "ValidateUser" )]
        [HttpPost]
        [HttpOptions]
        public ONB.Models.Users UserLoginInfo( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        {
            LDAPHelper lHelper = new LDAPHelper();
            ONB.Models.Users user = lHelper.GetUserDetailsFromAD( objuser );
            user.Authenticated = userRepo.ValidateUserLogin( objuser );
            System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> lADgroups = lHelper.GetADgroups( objuser );
            foreach (ONB.Models.UserMemberOfThisADgroup group in lADgroups)
            {
                user.ADgroups = user.ADgroups + "," + group.UserMemberOfADgroupList.ToString();
            }
            return user;
        }


        //[HttpPost]
        //[HttpOptions]
        //public HttpResponseMessage ValidateUserLogin(Users objuser)
        //{
        //   // return Request.CreateResponse(userRepo.ValidateUserLogin(objuser));
        //}
        [Route( "ValidateUserCredentials" )]
        [HttpPost]
        [HttpOptions]
        public bool ValidateUserLogin( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        {
            bool lauth = userRepo.ValidateUserLogin( objuser );
            return lauth;

        }

        [Route( "UserAttributes" )]
        [HttpPost]
        [HttpOptions]
        public ONB.Models.Users GetUserAttributes( [FromBody] ONB.Models.Users objuser )
        {
            ONB.Models.Users lauth = userRepo.RepositoryGetUserAttributes( objuser );
            return lauth;

        }
        //[Route( "GetAstringV1" )]
        //[HttpPost]
        //[HttpOptions]
        //public string GetOneString( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        //{
        //    string lauth = userRepo.RepositoryGetOneString( objuser );
        //    return lauth;

        //}

        [Route( "TestForAuser" )]
        [HttpPost]
        [HttpOptions]
        public ONB.Models.Users GetOneString( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        {
            ONB.Models.Users testedUIDpwd = userRepo.RepositoryGetOneUser( objuser );
            return testedUIDpwd;

        }

        [Route( "UserADgroupMembership" )]
        [HttpPost]
        [HttpOptions]
        public System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> GetADgroups( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        {
            LDAPHelper lHelper = new LDAPHelper();
            System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> lADgroups = lHelper.GetADgroups( objuser );
            return lADgroups;
        }

        [Route( "IsAuthorizedONBstaff" )]
        [HttpPost]
        [HttpOptions]
        public System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> GetONBauthorizations( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        {
            LDAPHelper lHelper = new LDAPHelper();
            System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> lADgroups = lHelper.GetONBauthorizations( objuser );
            return lADgroups;
        }



        [Route("IsAuthorizedSTDBstaff")]
        [HttpPost]
        [HttpOptions]
        public System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> GetSTDBauthorizations([FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser)
        {
            LDAPHelper lHelper = new LDAPHelper();
            System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> lADgroups = lHelper.GetSTDBauthorizations(objuser);
            return lADgroups;
        }

        [Route( "UserDetails" )]
        [HttpPost]
        [HttpOptions]
        public ONB.Models.Users GetUserDetails( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        {
            LDAPHelper lHelper = new LDAPHelper();
            ONB.Models.Users lADdetails = lHelper.GetUserDetailsFromAD( objuser );
            return lADdetails;
        }

        [Route( "AnyUserDetails" )]
        [HttpPost]
        [HttpOptions]
        public ONB.Models.Users GetAnyUserDetails( [FromBody] ONB.Helper.LDAPHelper.ValidateUser objuser )
        { // send any user id. If it exists in DSNYAD, Users will have current voicemail phone number, email, REF#
            LDAPHelper lHelper = new LDAPHelper();
            ONB.Models.Users lADdetails = lHelper.GetAnyUserDetailsFromAD( objuser ); // PWD doesn't matter
            return lADdetails;
        }

        [Route( "AnyRefNoDetails/{aRefNo}" )]
        [HttpGet]
        [HttpOptions]
        public ONB.Models.Users GetAnyUserDetails( string aRefNo )
        { // send any user id. If it exists in DSNYAD, Users will have current voicemail phone number, email, REF#
            LDAPHelper lHelper = new LDAPHelper();
            ONB.Models.Users lADdetails = lHelper.GetAnyRefNoDetailsFromAD( aRefNo ); // PWD doesn't matter
            return lADdetails;
        }
    }
}