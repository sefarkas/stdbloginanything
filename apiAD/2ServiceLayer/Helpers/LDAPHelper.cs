﻿using System.Net;


// https://stackoverflow.com/questions/34319263/return-a-list-of-all-active-directory-groups-a-user-belongs-to-in-string
// https://stackoverflow.com/questions/4460558/how-to-get-all-the-ad-groups-for-a-particular-user
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Security;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using System.DirectoryServices.Protocols;
using ONB.DataAccess;
using ONB.Models;
using System.Collections;
using System;
using System.Security.Principal;
using System.DirectoryServices.AccountManagement;
using System.Linq;

namespace ONB.Helper
{
    public class LDAPHelper
    {
        private static IConfigurationRoot Configuration
        {
            get; set;
        }

        #region Enums subtypes

        public enum ErrorCodes
        {
            InvalidUser,
            UnableToProcessUser,
            AccountLocked,
            ValidUser
        }

        #endregion
        public class ValidateUser
        {
            public string UserName
            {
                get; set;
            }
            public string Password
            {
                get; set;
            }
        }
        /// <summary>
        /// Authenticate user using LDAP 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>returns error code</returns>
        public bool ValidateUserCredentialsEpickup( ValidateUser validateThisUser )
        {
            string testUserName = validateThisUser.UserName + "";
            string testPassword = validateThisUser.Password + "";

            if (testUserName.Length + testPassword.Length == 0)
                throw new System.Exception( "User ID and PWD are both empty." );
            else if (testPassword.Length == 0)
                throw new System.Exception( "PWD not provided." );
            else if (testUserName.Length == 0)
                throw new System.Exception( "User ID not provided. Send abc rather than dsnyad\abc or abc@dsny.nyc.gov." );


            var builder = new ConfigurationBuilder()
                .SetBasePath( Directory.GetCurrentDirectory() )
                .AddJsonFile( "appsettings.json" )
                .AddJsonFile( "appsettings.Development.json" );

            Configuration = builder.Build();

            string LDAPServer = Configuration["AppSettings:LDAPServer"]; // ConfigurationManager.AppSettings["LDAPServer"];
            string LDAPPath = Configuration["AppSettings:LDAPPath"]; // .AppSettings["LDAPPath"];
            string LDAPOrg = Configuration["AppSettings:LDAPOrg"]; // ConfigurationManager.AppSettings["LDAPOrg"];

            string msg = "LDAP strings: " + LDAPServer + ";" + LDAPPath + ";" + LDAPOrg;
            Common.LogException( msg );

            string FirstName = string.Empty;
            string LastName = string.Empty;

            bool isAuthenticated = false;
            bool checkLoop = false;

            try
            {
                using (SecureString pass = new SecureString())
                {
                    for (int i = 0; i < validateThisUser.Password.Length; i++)
                        pass.AppendChar( (char)validateThisUser.Password[i] );
                    msg = "PWDlength: " + pass.Length.ToString();
                    Common.LogException( msg );
                    using (System.DirectoryServices.DirectoryEntry entry = new System.DirectoryServices.DirectoryEntry( LDAPPath + LDAPOrg, null, null, AuthenticationTypes.None ))
                    {
                        if (entry.Path != null)
                        {
                            msg = "DirectoryEntry: " + entry.Path;

                        }
                        else
                        {
                            msg = "DirectoryEntry: entry name is null.";
                        }
                        Common.LogException( msg );

                        DirectorySearcher search = new DirectorySearcher( entry );
                        msg = "DirectorySearcher: " + search.SearchRoot.ToString();
                        Common.LogException( msg );


                        search.SearchScope = System.DirectoryServices.SearchScope.Subtree;
                        // search.Filter = "(&(objectclass=inetorgperson)(uid=" + testUserName + "))";
                        // search.Filter = string.Format( "(&(objectClass=inetorgperson)(objectCategory=user) (sAMAccountName={0}))", testUserName );
                        search.Filter = string.Format( "(&(objectClass=inetorgperson)(objectCategory=user) (uid={0}))", testUserName );
                        search.PageSize = int.MaxValue;

                        msg = "search.Filter: " + search.Filter;
                        Common.LogException( msg );

                        SearchResultCollection resultCollection = search.FindAll();
                        Common.LogException( "resultCollection created without incident." );

                        if (resultCollection != null && resultCollection.Count == 0)
                        {
                            search.Filter = string.Format( "(&(objectclass=inetorgperson)(objectCategory=user) (cn={0}))", testUserName );
                            msg = "search.Filter: " + search.Filter;
                            Common.LogException( msg );
                            resultCollection = search.FindAll();
                        }

                        msg = "try search.FindOne ";
                        Common.LogException( msg );

                        SearchResult aResult = search.FindOne();

                        search.Dispose();

                        //DirectoryEntry personEntry = aResult.GetDirectoryEntry();
                        //PropertyValueCollection ADgrps = personEntry.Properties["memberOf"];

                        // PropertyValueCollection ADgroups = GetBBGroups2( entry, validateThisUser.UserName );

                        string[] pipeOfADgroups = GetGroups( testUserName );
                        //foreach (string groupName in ADgrps)
                        //{
                        //    // Console.WriteLine( groupName );
                        //    pipeOfADgroups = pipeOfADgroups + "|" + groupName;
                        //}
                        //Console.WriteLine( pipeOfADgroups.ToString() );
                        msg = "search.pipeOfADgroups: " + pipeOfADgroups.ToString();
                        Common.LogException( msg );

                        if (resultCollection != null)
                        {
                            msg = "resultCollection has item(s) ";
                            Common.LogException( msg );
                            for (int counter = 0; counter <
                                     resultCollection.Count; counter++)
                            {
                                if (resultCollection[counter].Properties.Contains( "givenname" ))
                                {
                                    FirstName = resultCollection[counter].Properties["givenname"][0].ToString();
                                }
                                if (resultCollection[counter].Properties.Contains( "sn" ))
                                {
                                    LastName = resultCollection[counter].Properties["sn"][0].ToString();
                                }
                                if (resultCollection[counter].Properties.Contains( "mail" ))
                                {
                                    string Name = resultCollection[counter].Properties["mail"][0].ToString();
                                }
                                if (resultCollection[counter].Properties.Contains( "description" ))
                                {
                                    string Name = resultCollection[counter].Properties["description"][0].ToString();
                                }
                                if (resultCollection[counter].Properties.Contains( "groupmembership" ))
                                {
                                    int howmany = resultCollection[counter].Properties["groupmembership"].Count;
                                    string firstGroup = resultCollection[counter].Properties["groupmembership"][0].ToString();
                                }
                            }
                        }

                        //try to autneticate the user
                        while (!checkLoop)
                        {
                            int resultCounter = 0;
                            try
                            {
                                SearchResult result = resultCollection[0];
                                msg = "try result.GetDirectoryEntry() ";
                                Common.LogException( msg );

                                System.DirectoryServices.DirectoryEntry person = result.GetDirectoryEntry();
                                string DN = person.Path.Remove( 0, LDAPPath.Length );

                                NetworkCredential LDAPCredential = new NetworkCredential( DN, Marshal.PtrToStringUni( Marshal.SecureStringToBSTR( pass ) ) );
                                System.DirectoryServices.Protocols.LdapConnection LDAPConn = new System.DirectoryServices.Protocols.LdapConnection( new LdapDirectoryIdentifier( LDAPServer ), null, AuthType.Basic );

                                try
                                {
                                    msg = "try LDAPConn.Bind(LDAPCredential) ";
                                    Common.LogException( msg );
                                    LDAPConn.Bind( LDAPCredential );
                                    isAuthenticated = true;
                                    checkLoop = true;
                                }
                                catch (System.DirectoryServices.Protocols.LdapException ex)
                                {
                                    //if in here recieved an error so not authenticated against edirectory
                                    switch (ex.ErrorCode)
                                    {
                                        case 49:
                                            isAuthenticated = false;
                                            break;
                                        default:
                                            isAuthenticated = false;
                                            break;
                                    }
                                    Common.LogException( ex );
                                    checkLoop = true;
                                }
                                catch (DirectoryOperationException DirOpsEx)
                                {
                                    //if in here recieved there is an account lockout
                                    isAuthenticated = false;
                                    Common.LogException( DirOpsEx );
                                    checkLoop = true;
                                }
                                finally
                                {
                                    LDAPConn.Dispose();
                                }

                                resultCounter++;
                            }
                            catch (System.Exception ex)
                            {
                                isAuthenticated = false;
                                Common.LogException( ex );
                                checkLoop = true;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString() + ex.StackTrace.ToString();
                isAuthenticated = false;
                Common.LogException( msg );
            }
            return isAuthenticated;
        }

        public bool ValidateUserCredentials( ValidateUser validateThisUser )
        {
            string testUserName = validateThisUser.UserName + "";
            string testPassword = validateThisUser.Password + "";

            if (testUserName.Length + testPassword.Length == 0)
                throw new System.Exception( "User ID and PWD are both empty." );
            else if (testPassword.Length == 0)
                throw new System.Exception( "PWD not provided." );
            else if (testUserName.Length == 0)
                throw new System.Exception( "User ID not provided. Send abc rather than dsnyad\abc or abc@dsny.nyc.gov." );

            bool isAuthenticated = false;
            string msg = "";
            try
            {
                using (var ctx = new PrincipalContext( ContextType.Domain ))
                using (UserPrincipal user = UserPrincipal.FindByIdentity( ctx, testUserName ))
                {
                    if (user != null)
                    {
                        if (ctx.ValidateCredentials( testUserName, testPassword ))
                        {
                            isAuthenticated = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString() + ex.StackTrace.ToString();
                isAuthenticated = false;
                Common.LogException( msg );
            }
            return isAuthenticated;
        }


        public Users GetUserDetailsFromAD( ValidateUser validateThisUser )
        {
            string testUserName = validateThisUser.UserName + "";
            string testPassword = validateThisUser.Password + "";

            if (testUserName.Length + testPassword.Length == 0)
                throw new System.Exception( "User ID and PWD are both empty." );
            else if (testPassword.Length == 0)
                throw new System.Exception( "PWD not provided." );
            else if (testUserName.Length == 0)
                throw new System.Exception( "User ID not provided. Send abc rather than dsnyad\abc or abc@dsny.nyc.gov." );

            string msg = "";
            Users aUserClass = new Users();
            try
            {
                using (var ctx = new PrincipalContext( ContextType.Domain ))
                using (UserPrincipal user = UserPrincipal.FindByIdentity( ctx, testUserName ))
                {
                    if (user != null)
                    {
                        if (ctx.ValidateCredentials( testUserName, testPassword ))
                        {
                            aUserClass.Email = user.EmailAddress;
                            aUserClass.FirstName = user.GivenName;
                            aUserClass.LastName = user.Surname;
                            aUserClass.UserName = user.SamAccountName;
                            aUserClass.UserId = Convert.ToInt32( user.EmployeeId );
                            aUserClass.VoiceTelephoneNumber = user.VoiceTelephoneNumber;
                            aUserClass.AuthenticationMessage = "Valid";
                        }
                        else
                        {
                            aUserClass.AuthenticationMessage = "Invalid";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString() + ex.StackTrace.ToString();
                aUserClass.UserName = msg;
                Common.LogException( msg );
            }
            return aUserClass;
        }
        public Users GetAnyUserDetailsFromAD( ValidateUser validateThisUser )
        {
            string testUserName = validateThisUser.UserName + "";
            string testPassword = validateThisUser.Password + "";

            if (testUserName.Length == 0)
                throw new System.Exception( "User ID not provided. Send abc rather than dsnyad\abc or abc@dsny.nyc.gov." );

            string msg = "";
            Users aUserClass = new Users();
            try
            {
                using (var ctx = new PrincipalContext( ContextType.Domain ))
                using (UserPrincipal user = UserPrincipal.FindByIdentity( ctx, testUserName ))
                {
                    if (user != null)
                    {
                        aUserClass.Email = user.EmailAddress;
                        aUserClass.FirstName = user.GivenName;
                        aUserClass.LastName = user.Surname;
                        aUserClass.UserName = user.SamAccountName;
                        aUserClass.UserId = Convert.ToInt32( user.EmployeeId );
                        aUserClass.VoiceTelephoneNumber = user.VoiceTelephoneNumber;
                        aUserClass.AuthenticationMessage = "not checked";
                    }
                }
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString() + ex.StackTrace.ToString();
                aUserClass.UserName = msg;
                Common.LogException( msg );
            }
            return aUserClass;
        }
        public Users GetAnyRefNoDetailsFromAD( string RefNo )
        {
            string testUserName = RefNo + "";

            if (testUserName.Length == 0)
                throw new System.Exception( "REF# not provided. Send seven digit employee reference number." );

            string msg = "";
            Users aUserClass = new Users();
            try
            {
                using (var ctx = new PrincipalContext( ContextType.Domain ))
                using (UserPrincipal searchTemplate = new UserPrincipal( ctx ))
                {
                    searchTemplate.EmployeeId = RefNo;
                    using (PrincipalSearcher ps = new PrincipalSearcher( searchTemplate ))
                    using (UserPrincipal user = (UserPrincipal)ps.FindOne())
                    {
                        if (user != null)
                        {
                            aUserClass.Email = user.EmailAddress;
                            aUserClass.FirstName = user.GivenName;
                            aUserClass.LastName = user.Surname;
                            aUserClass.UserName = user.SamAccountName;
                            aUserClass.UserId = Convert.ToInt32( user.EmployeeId );
                            aUserClass.VoiceTelephoneNumber = user.VoiceTelephoneNumber;
                            aUserClass.AuthenticationMessage = "not checked";
                        }
                        else
                        {
                            aUserClass.AuthenticationMessage = "cannot find " + RefNo;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString() + ex.StackTrace.ToString();
                aUserClass.UserName = msg;
                Common.LogException( msg );
            }
            return aUserClass;
        }
        public ONB.Models.Users GetUserAttributesHelper( ONB.Models.Users userDetails )
        {
            string testUserName = userDetails.UserName + "";
            string testPassword = userDetails.Password + "";

            if (testUserName.Length + testPassword.Length == 0)
                throw new System.Exception( "User ID and PWD are both empty." );
            else if (testPassword.Length == 0)
                throw new System.Exception( "PWD not provided." );
            else if (testUserName.Length == 0)
                throw new System.Exception( "User ID not provided. Send abc rather than dsnyad\abc or abc@dsny.nyc.gov." );

            string msg = "";
            try
            {
                using (var ctx = new PrincipalContext( ContextType.Domain ))
                using (UserPrincipal user = UserPrincipal.FindByIdentity( ctx, testUserName ))
                {
                    if (user != null)
                    {
                        if (ctx.ValidateCredentials( testUserName, testPassword ))
                        {

                            userDetails.Email = user.EmailAddress;
                            userDetails.FirstName = user.GivenName;
                            userDetails.LastName = user.Surname;
                            userDetails.LastLoginDate = DateTime.Now; // local time of the API server
                            userDetails.UserRoles = new System.Collections.Generic.List<UserRoleType>();
                            userDetails.AuthenticationMessage = "Valid";
                        }
                        else
                        {
                            userDetails.AuthenticationMessage = "Invalid";
                            return userDetails;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                msg = ex.Message.ToString() + ex.StackTrace.ToString();
                Common.LogException( msg );
                userDetails.AuthenticationMessage = msg;
            }
            userDetails.Password = "re-enter it";
            return userDetails;
        }

        public string GetADPrincipalGroupMembership( DirectorySearcher search )
        {
            search.PropertiesToLoad.Add( "memberOf" );
            System.Text.StringBuilder groupNames = new System.Text.StringBuilder(); //stuff them in | delimited

            SearchResult result = search.FindOne();
            int propertyCount = result.Properties["memberOf"].Count;
            System.String dn;
            int equalsIndex, commaIndex;

            for (int propertyCounter = 0; propertyCounter < propertyCount;
                propertyCounter++)
            {
                dn = (System.String)result.Properties["memberOf"][propertyCounter];

                equalsIndex = dn.IndexOf( "=", 1 );
                commaIndex = dn.IndexOf( ",", 1 );
                if (-1 == equalsIndex)
                {
                    return null;
                }
                groupNames.Append( dn.Substring( (equalsIndex + 1),
                            (commaIndex - equalsIndex) - 1 ) );
                groupNames.Append( "|" );
            }

            return groupNames.ToString();
        }

        public PropertyValueCollection GetBBGroups2( System.DirectoryServices.DirectoryEntry objEntry, string UserID )
        {
            PropertyValueCollection ADgrps = null;
            try
            {
                String userName = UserID;
                int pos = userName.IndexOf( @"\" );
                if (pos > 0)
                    userName = userName.Substring( pos + 1 );

                PrincipalContext domain = new PrincipalContext( ContextType.Domain, "dsnyad.nycnet" );
                UserPrincipal user = UserPrincipal.FindByIdentity( domain, IdentityType.SamAccountName, userName );

                // DirectoryEntry de = new DirectoryEntry( "LDAP://RIOMC.com" ); // LDAPPath
                // DirectoryEntry de = new DirectoryEntry( LDAPPath ); // LDAPPath
                DirectorySearcher personSearcher = new DirectorySearcher( objEntry );
                personSearcher.Filter = string.Format( "(SAMAccountName={0}", userName );
                SearchResult result = personSearcher.FindOne();
                personSearcher.Dispose();
                if (result != null)
                {
                    DirectoryEntry personEntry = result.GetDirectoryEntry();
                    ADgrps = personEntry.Properties["memberOf"];
                    //foreach (string g in ADgrps)
                    //{
                    //    Console.WriteLine( g ); // will write group name
                    //}
                    return ADgrps;
                }
            }
            catch
            {
                // return an empty list...
                throw;
            }

            return ADgrps;
        }

        public static string GetOneString( string username )
        {
            return "sample string from on board API";
        }

        public ONB.Models.Users GetAuthenticationMessage( ValidateUser validateThisUser )
        {
            ONB.Models.Users aUser = new ONB.Models.Users();
            if (ValidateUserCredentials( validateThisUser ))
                aUser.AuthenticationMessage = "Valid";
            else
                aUser.AuthenticationMessage = "Invalid";

            return aUser;
        }

        public static string[] GetGroups( string username )
        {
            string[] output = null;

            using (var ctx = new PrincipalContext( ContextType.Domain ))
            using (var user = UserPrincipal.FindByIdentity( ctx, username ))
            {
                if (user != null)
                {
                    output = user.GetGroups() //this returns a collection of principal objects
                        .Select( x => x.SamAccountName ) // select the name.  you may change this to choose the display name or whatever you want
                        .ToArray(); // convert to string array
                }
            }

            return output;
        }
        public System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> GetADgroups( ONB.Helper.LDAPHelper.ValidateUser pObjUsername )
        { // ignores PWD
            string pUsername = pObjUsername.UserName;
            string[] arrOutput = null;
            // prepare response list
            System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> lADgroups = new System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup>();

            using (var ctx = new PrincipalContext( ContextType.Domain )) // domain where this API is running -- assumed dsnyad.nycnet
            using (var user = UserPrincipal.FindByIdentity( ctx, pUsername )) // assumes a legit UID in dsnyad.nycnet domain
            {
                if (user != null)
                {
                    try
                    {
                        arrOutput = user.GetGroups() //this returns a collection of principal objects
                            .Select( x => x.SamAccountName ) // select the name.  you may change this to choose the display name or whatever you want
                            .ToArray(); // convert to string array
                    }
                    catch (Exception e)
                    {
                        string err = e.Message;
                        if (e.InnerException != null)
                        {
                            err += e.InnerException.Message.ToString();
                        }
                        UserMemberOfThisADgroup loneADgroup = new UserMemberOfThisADgroup
                        {
                            UserName = pUsername,
                            UserMemberOfADgroupList = err + " for " + pUsername
                        };
                        lADgroups.Add( loneADgroup ); // build up the list of AD Groups pUsername is a member of
                        return lADgroups;

                    }
                }
                else
                {
                    UserMemberOfThisADgroup loneADgroup = new UserMemberOfThisADgroup
                    {
                        UserName = pUsername,
                        UserMemberOfADgroupList = "system found no AD groups for " + pUsername
                    };
                    lADgroups.Add( loneADgroup ); // build up the list of AD Groups pUsername is a member of
                    return lADgroups;
                }
            }


            // did AD return any groups?
            if (arrOutput.Length > 0)
            {
                ArrayList output = new ArrayList( arrOutput );  // may not be necessary
                try
                {
                    if (output.Count > 0)// test for ArrayList versus .Length for an array
                    {
                        foreach (string item in output) // output only has the group names 
                        {
                            UserMemberOfThisADgroup loneADgroup = new UserMemberOfThisADgroup
                            {
                                UserName = pUsername,
                                UserMemberOfADgroupList = item // one AD group name as a string
                            };
                            lADgroups.Add( loneADgroup ); // build up the list of AD Groups pUsername is a member of
                        }
                    }
                }
                catch (Exception ex)
                {
                    Common.LogException( ex );
                    throw;
                }
            }

            return lADgroups; // returns JSON with username/ADgroup pairs, username always the same
        }

        public ArrayList GetBBGroups( System.DirectoryServices.DirectoryEntry de, string UserID )
        {
            ArrayList groups = new ArrayList();

            try
            {
                String userName = UserID;
                int pos = userName.IndexOf( @"\" );
                if (pos > 0)
                    userName = userName.Substring( pos + 1 );

                PrincipalContext domain = new PrincipalContext( ContextType.Domain, "dsnyad.nycnet" );
                UserPrincipal user = UserPrincipal.FindByIdentity( domain, IdentityType.SamAccountName, userName );

                // DirectoryEntry de = new DirectoryEntry( "LDAP://RIOMC.com" ); // LDAPPath
                // DirectoryEntry de = new DirectoryEntry( LDAPPath ); // LDAPPath
                DirectorySearcher search = new DirectorySearcher( de );
                search.Filter = "(&(objectClass=group)(member=" + user.DistinguishedName + "))";
                search.PropertiesToLoad.Add( "samaccountname" );
                search.PropertiesToLoad.Add( "cn" );

                String name;
                SearchResultCollection results = search.FindAll();

                search.Dispose();

                foreach (SearchResult result in results)
                {
                    name = (String)result.Properties["samaccountname"][0];
                    if (String.IsNullOrEmpty( name ))
                    {
                        name = (String)result.Properties["cn"][0];
                    }
                    GetGroupsRecursive( groups, de, name );
                }
            }
            catch
            {
                // return an empty list...
                throw;
            }

            return groups;
        }

        public void GetGroupsRecursive( ArrayList groups, DirectoryEntry de, String dn )
        {
            DirectorySearcher search = new DirectorySearcher( de );
            search.Filter = "(&(objectClass=group)(|(samaccountname=" + dn + ")(cn=" + dn + ")))";
            search.PropertiesToLoad.Add( "memberof" );

            String group, name;
            SearchResult result = search.FindOne();
            search.Dispose();

            if (result == null)
                return;

            group = @"RIOMC\" + dn;
            if (!groups.Contains( group ))
            {
                groups.Add( group );
            }
            if (result.Properties["memberof"].Count == 0)
                return;
            int equalsIndex, commaIndex;
            foreach (String dn1 in result.Properties["memberof"])
            {
                equalsIndex = dn1.IndexOf( "=", 1 );
                if (equalsIndex > 0)
                {
                    commaIndex = dn1.IndexOf( ",", equalsIndex + 1 );
                    name = dn1.Substring( equalsIndex + 1, commaIndex - equalsIndex - 1 );
                    GetGroupsRecursive( groups, de, name );
                }
            }
        }
        public System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> GetSTDBauthorizations(ONB.Helper.LDAPHelper.ValidateUser pObjUsername)
        { // ignores PWD
            string pUsername = pObjUsername.UserName;
            string[] arrOutput = null;
            // prepare response list
            System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> lADgroups = new System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup>();

            using (var ctx = new PrincipalContext(ContextType.Domain)) // domain where this API is running -- assumed dsnyad.nycnet
            using (var user = UserPrincipal.FindByIdentity(ctx, pUsername)) // assumes a legit UID in dsnyad.nycnet domain
            {
                if (user != null)
                {
                    arrOutput = user.GetGroups() //this returns a collection of principal objects
                        .Select(x => x.SamAccountName) // select the name.  you may change this to choose the display name or whatever you want
                        .ToArray(); // convert to string array
                }
                else
                {
                    UserMemberOfThisADgroup loneADgroup = new UserMemberOfThisADgroup
                    {
                        UserName = pUsername,
                        UserMemberOfADgroupList = "system found no AD groups for " + pUsername
                    };
                    lADgroups.Add(loneADgroup); // build up the list of AD Groups pUsername is a member of
                    return lADgroups;
                }
            }


            // did AD return any groups?
            if (arrOutput.Length > 0)
            {
                ArrayList output = new ArrayList(arrOutput);  // may not be necessary
                try
                {
                    string pattern1 = "DST";
                    string pattern2 = "FBF_DataEase_Access";

                    if (output.Count > 0)// test for ArrayList versus .Length for an array
                    {
                        foreach (string item in output) // output only has the group names 
                        {
                            if (item.Contains(pattern1) | item.Contains(pattern2))
                            {
                                UserMemberOfThisADgroup loneADgroup = new UserMemberOfThisADgroup
                                {
                                    UserName = pUsername,
                                    UserMemberOfADgroupList = item // one AD group name as a string
                                };
                                lADgroups.Add(loneADgroup); // build up the list of AD Groups pUsername is a member of
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Common.LogException(ex);
                    throw;
                }
            }

            return lADgroups; // returns JSON with username/ADgroup pairs, username always the same
        }

        public System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> GetONBauthorizations( ONB.Helper.LDAPHelper.ValidateUser pObjUsername )
        { // ignores PWD
            string pUsername = pObjUsername.UserName;
            string[] arrOutput = null;
            // prepare response list
            System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup> lADgroups = new System.Collections.Generic.List<ONB.Models.UserMemberOfThisADgroup>();

            using (var ctx = new PrincipalContext( ContextType.Domain )) // domain where this API is running -- assumed dsnyad.nycnet
            using (var user = UserPrincipal.FindByIdentity( ctx, pUsername )) // assumes a legit UID in dsnyad.nycnet domain
            {
                if (user != null)
                {
                    arrOutput = user.GetGroups() //this returns a collection of principal objects
                        .Select( x => x.SamAccountName ) // select the name.  you may change this to choose the display name or whatever you want
                        .ToArray(); // convert to string array
                }
                else
                {
                    UserMemberOfThisADgroup loneADgroup = new UserMemberOfThisADgroup
                    {
                        UserName = pUsername,
                        UserMemberOfADgroupList = "system found no AD groups for " + pUsername
                    };
                    lADgroups.Add( loneADgroup ); // build up the list of AD Groups pUsername is a member of
                    return lADgroups;
                }
            }


            // did AD return any groups?
            if (arrOutput.Length > 0)
            {
                ArrayList output = new ArrayList( arrOutput );  // may not be necessary
                try
                {
                    string pattern1 = "SCCM_HelpDesk";
                    string pattern2 = "SCCM_Support_L2";

                    if (output.Count > 0)// test for ArrayList versus .Length for an array
                    {
                        foreach (string item in output) // output only has the group names 
                        {
                            if (item.Contains(pattern1) | item.Contains( pattern2 ))
                            {
                                UserMemberOfThisADgroup loneADgroup = new UserMemberOfThisADgroup
                                {
                                    UserName = pUsername,
                                    UserMemberOfADgroupList = item // one AD group name as a string
                                };
                                lADgroups.Add( loneADgroup ); // build up the list of AD Groups pUsername is a member of
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Common.LogException( ex );
                    throw;
                }
            }

            return lADgroups; // returns JSON with username/ADgroup pairs, username always the same
        }
        private static bool substr( string input, string matchAnyPartOfInputWithThis )
        {
            //Create temp string  
            //string temp = "";
            // bool rtn = false;
            string pattern = @"/pattern/g";
            pattern.Replace( "pattern", matchAnyPartOfInputWithThis );
            //Console.WriteLine( "pattern is {0}", pattern );
            //Console.WriteLine( "input is {0}", input );
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex( pattern, System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.IgnoreCase );
            System.Text.RegularExpressions.MatchCollection matches = rx.Matches( input );

            //for (int i = 0, k = 0; i < input.Length; i++)
            //{
            //    //Match the value char by char of both string  
            //    if (input[i] == matchAnyPartOfInputWithThis[k])
            //    {
            //        //if matches then add the char to temp variable  
            //        temp += input[i];
            //        k++;
            //    }
            //    //If matching string completed  
            //    if (k == matchAnyPartOfInputWithThis.Length)
            //        break;
            //}
            ////return the true/false on basis of matching  
            //return matchAnyPartOfInputWithThis.Equals( temp );

            //Console.WriteLine( "matches is {0}", matches.Count );
            return matches.Count > 0 ? true : false ;

        }
    }
}
